//
//  ViewController.swift
//  TableView
//
//  Created by burak on 22.02.2020.
//  Copyright © 2020 burak. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var products = ["Masa","Sandalye","TV","MacbookPro","Iphone 11 Pro Max"]
    var prices = ["100 TL", "300 TL", "1000 TL" , "9200 TL", "11000 TL"]
    var stocks = ["50","400", "15", "25", "15" ]
    var images = ["masa","sandalye", "tv", "mac", "iphone"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
//    table satir sayisi
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! ProductTableViewCell
        cell.productImage.image = UIImage(named: "\(images[indexPath.row])")
        cell.productName.text = products[indexPath.row]
        cell.productPrice.text = prices[indexPath.row]
        cell.productStock.text = stocks[indexPath.row]
        cell.remainTime.text = "Last 4 hours"
        return cell
    }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 150
    }
}

